import visa
import time
import csv
import urllib2
import sys
import calendar
import socket

# start of Untitled
rm = visa.ResourceManager()

########################################
# Define a function to send an HTTP command and get the result
########################################
def Get_HTTP_Result(CmdToSend):
# Specify the IP address
    CmdToSend = "http://10.0.10.76/:" + CmdToSend
    # Send the HTTP command and try to read the result
    try:
        HTTP_Result = urllib2.urlopen(CmdToSend)
        PTE_Return = HTTP_Result.read()
        # The switch displays a web GUI for unrecognised commands
        if len(PTE_Return) > 100:
          print "Error, command not found:", CmdToSend
          PTE_Return = "Invalid Command!"
    # Catch an exception if URL is incorrect (incorrect IP or disconnected)
    except:
        print "Error, no response from device; check IP address and connections."
        PTE_Return = "No Response!"
        sys.exit() # Exit the script
    # Return the response
    return PTE_Return

##################
# Omega iServer Dual Temp Sensor
# This function polls and returns the temperature

# Connect the socket to the port where the server is listening
server_address = ('10.0.10.117', 1000)
message = '*SRHC\r'


# print 'connecting to %s port %s' % server_address
# sock.close()
# sock.connect(server_address)
def Get_Temp():
    try:
        print 'connecting to %s port %s' % server_address
        # sock.close()
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(server_address)
        # Send data

        print 'sending "%s"' % message
        sock.sendall(message)

        # Look for the response
        amount_received = 0
        amount_expected = len(message)

        while amount_received < amount_expected:
            data = sock.recv(16)
            amount_received += len(data)
            print 'Temp: "%s"' % data


    finally:
        print 'closing socket'
        sock.close()
    return data


count = 1
nowtim=calendar.timegm(time.gmtime())
RunName = input("Enter name for data file: ") or 'default-file'
RunName = RunName + nowtim

with open( RunName + '.csv', 'a',0) as csvfile:
    fieldnames = ['chan1Mkr','chan2Mkr','chan3Mkr','chan4Mkr','ChamTemp','curr','volt','time','timesec']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    print ("Mar2018-AutoPowerOn wSleeps - E4440A: %s") %RunName

    # Initialize instruments
    rm = visa.ResourceManager()
    #Temp Sensor
    #v34405A = rm.open_resource('USB0::0x0957::0x0618::MY53090034::0::INSTR')
    # Power Supply / DMM
    U3606B = rm.open_resource('GPIB0::25::INSTR')
    # E4440A Spec Ann
    E4440A = rm.open_resource('TCPIP0::10.0.10.49::inst0::INSTR')
    E4440A.timeout = 50000
    E4440A.write(':CALCulate:MARKer1:FCOunt ON')    #Turn on Marker Counter
    # 11713C Switch/Attenuator Controller
    v11713C = rm.open_resource('TCPIP0::10.0.10.67::inst0::INSTR')

    # Set up Power Supply
    U3606B.write(':OUTPut:STATe %d' % (0))
    temp_values = U3606B.query_ascii_values(':OUTPut:STATe?')
    state = int(temp_values[0])
    U3606B.write(':OUTPut:STATe %d' % (1))
    temp_values = U3606B.query_ascii_values(':OUTPut:STATe?')
    state1 = int(temp_values[0])
    print("PS state= %d") %state1
    time.sleep(1)
    temp_values = U3606B.query_ascii_values(':SOURce:SENSe:CURRent:LEVel?')
    current = temp_values[0]
    print("current= %f") %current
    temp_values = U3606B.query_ascii_values(':SOURce:SENSe:VOLTage:LEVel?')
    voltage = temp_values[0]
    print("voltage= %f") %voltage

    # Set up initial state of switches
    v11713C.write(':ROUTe:OPEn:ALL')
    v11713C.write(':ROUTe:CLOSe (%s)' % ('@208'))
    # Now everything is off
    #Turn on 10 MHz
    v11713C.write(':ROUTe:CLOSe (%s)' % ('@110'))
    #Turn on LNB 1
    v11713C.write(':ROUTe:CLOSe (%s)' % ('@201'))

    
    time_val = time.asctime( time.localtime(time.time()))
    starttime=calendar.timegm(time.gmtime())
    print("Start Time: %s") %time_val
          
    ##Do one cycle with power on
    for x in range(1, 5):
        print "We're on time %d" % (x)
        ########################################
        # Send some commands to the switch box
        ########################################

        if x == 1:
            #status = Get_HTTP_Result("SP4TA:STATE:1") # Set switch A to position x
            v11713C.write(':ROUTe:CLOSe (%s)' % ('@201')) # Set LNB 1 on
            time.sleep(2)
            E4440A.write(':CALCulate:MARKer1:ACTivate')
            E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
            E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
            level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
            print ("Level: %f") %level
            if level < -40:
                time.sleep(1)
                E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
                level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
                if level < -40:
                    #E4440A.write(':MMEMory:CDIRectory "%s"' % ('[USBDISK]:'))
                    Label = (RunName + '_%d_%d_low',(x,count))
                    print (" _%d_low: %d ************" % (x,level))
                    print Label
                    #E4440A.write(':MMEMory:STORe:IMAGe "%s"' % (str(Label)+'.png'))
            time.sleep(2)
##                marker1 = float(E4440A.query_ascii_values(':CALC:MARK1:FCO:X?'))
            for y in range(1, 5):
                marker1 = float(E4440A.query(':CALC:MARK1:FCO:X?'))
                time.sleep(1)
                if marker1 > 1500000005:
                    print "------------------ FAIL -------------------"
            #marker1 = float(temp_values[0])
                print marker1
            v11713C.write(':ROUTe:OPEn (%s)' % ('@201'))  # Set LNB 1 off
        if x == 2:
            #status = Get_HTTP_Result("SP4TA:STATE:2") # Set switch A to position x
            v11713C.write(':ROUTe:CLOSe (%s)' % ('@202'))  # Set LNB 2 on
            time.sleep(2)
            E4440A.write(':CALCulate:MARKer1:ACTivate')
            E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
            E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
            level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
            print ("Level: %f") %level
            if level < -40:
                time.sleep(1)
                E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
                level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
                if level < -40:
                    #E4440A.write(':MMEMory:CDIRectory "%s"' % ('[USBDISK]:'))
                    Label = (RunName + '_%d_%d_low',(x,count))
                    print (" _%d_low: %d ************" % (x,level))
                    print Label
                    #E4440A.write(':MMEMory:STORe:IMAGe "%s"' % (str(Label)+'.png'))
            time.sleep(2)
##                marker2 = float(E4440A.query_ascii_values(':CALC:MARK1:FCO:X?'))
            for y in range(1, 5):
                marker2 = float(E4440A.query(':CALC:MARK1:FCO:X?'))
                time.sleep(3)
                if marker2 > 1500000005:
                    print "------------------ FAIL -------------------"
            #marker1 = float(temp_values[0])
                print marker2
            v11713C.write(':ROUTe:OPEn (%s)' % ('@202'))  # Set LNB 2 off
        if x == 3:
            #status = Get_HTTP_Result("SP4TA:STATE:3") # Set switch A to position x
            v11713C.write(':ROUTe:CLOSe (%s)' % ('@203'))  # Set LNB 3 on
            time.sleep(2)
            E4440A.write(':CALCulate:MARKer1:ACTivate')
            E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
            E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
            level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
            print ("Level: %f") %level
            if level < -40:
                time.sleep(1)
                E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
                level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
                if level < -40:
                    #E4440A.write(':MMEMory:CDIRectory "%s"' % ('[USBDISK]:'))
                    Label = (RunName + '_%d_%d_low',(x,count))
                    print (" _%d_low: %d ************" % (x,level))
                    print Label
                    #E4440A.write(':MMEMory:STORe:IMAGe "%s"' % (str(Label)+'.png'))
            time.sleep(2)
##                marker3 = float(E4440A.query_ascii_values(':CALC:MARK1:FCO:X?'))
            for y in range(1, 5):
                marker3 = float(E4440A.query(':CALC:MARK1:FCO:X?'))
                time.sleep(1)
                if marker3 > 1500000005:
                    print "------------------ FAIL -------------------"                
                #marker1 = float(temp_values[0])
                print marker3
            v11713C.write(':ROUTe:OPEn (%s)' % ('@203'))  # Set LNB 3 off
        if x == 4:
            #status = Get_HTTP_Result("SP4TA:STATE:4") # Set switch A to position x
            v11713C.write(':ROUTe:CLOSe (%s)' % ('@204'))  # Set LNB 4 on
            time.sleep(2)
            E4440A.write(':CALCulate:MARKer1:ACTivate')
            E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
            E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
            level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
            print ("Level: %f") %level
            if level < -40:
                time.sleep(1)
                E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
                level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
                if level < -40:
                    #E4440A.write(':MMEMory:CDIRectory "%s"' % ('[USBDISK]:'))
                    Label = (RunName + '_%d_%d_low',(x,count))
                    print (" _%d_low: %d ************" % (x,level))
                    print Label
                    #E4440A.write(':MMEMory:STORe:IMAGe "%s"' % (str(Label)+'.png'))
            time.sleep(2)
##                marker4 = float(E4440A.query_ascii_values(':CALC:MARK1:FCO:X?'))
            for y in range(1, 5):
                marker4 = float(E4440A.query(':CALC:MARK1:FCO:X?'))
                time.sleep(1)
                if marker4 > 1500000005:
                    print "------------------ FAIL -------------------"     
                #marker1 = float(temp_values[0])
                print marker4
            v11713C.write(':ROUTe:OPEn (%s)' % ('@204'))  # Set LNB 4 off

            #############################################
            # Get Data for saving initial scan to file
            #
            nowtim=calendar.timegm(time.gmtime())
            time_val = time.asctime( time.localtime(time.time()))
            temp_values = U3606B.query_ascii_values(':SOURce:SENSe:CURRent:LEVel?')
            current = temp_values[0]
            print("current= %f") %current

            temp_values = U3606B.query_ascii_values(':SOURce:SENSe:VOLTage:LEVel?')
            voltage = temp_values[0]
            print("voltage= %f") %voltage
            temperature = Get_Temp()
            print ("Temp: %f") %temperature[0]
            writer.writerow({'chan1Mkr': marker1,'chan2Mkr': marker2,'chan3Mkr': marker3,'chan4Mkr': marker4,'ChamTemp':temperature[0],'curr':current,'volt':voltage,'time':time_val,'timesec':nowtim})    

##            status = Get_HTTP_Result("SP4TA:STATE:0") # Set switch A to position 0
##            print "Sw A connected, Com =>", Get_HTTP_Result("SP4TA:STATE?") # Print switch A position
        print "Done Initial Scan."
        time.sleep(5)
        
    U3606B.write(':OUTPut:STATe %d' % (0))
    temp_values = U3606B.query_ascii_values(':OUTPut:STATe?')
    state = int(temp_values[0])
    nowtim=calendar.timegm(time.gmtime())
    time_val = time.asctime( time.localtime(time.time()))   
    ##sleep for 2 hrs
    print("Turning off power and Soaking for 2 hrs")
    while((nowtim-starttime)<7200):
        nowtim=calendar.timegm(time.gmtime())
        temperature = Get_Temp()
        print ("Temp: %f") %temperature[0]
        time.sleep(5)
    #time.sleep(7200)
    print("Continuing")
    U3606B.write(':OUTPut:STATe %d' % (1))
    temp_values = U3606B.query_ascii_values(':OUTPut:STATe?')
    state1 = int(temp_values[0])
    print("PS state= %d") %state1
    
    while (1):
        #####
        ###turn off power supply for 20 seconds
        temperature = Get_Temp()
        print ("Temp: %f") %temperature[0]

        #Start main loop - poll the LNBs
        for x in range(1, 5):
            print "We're on time %d" % (x)
            ########################################
            # Send some commands to the switch box
            ########################################

            if x == 1:
                # status = Get_HTTP_Result("SP4TA:STATE:1") # Set switch A to position x
                v11713C.write(':ROUTe:CLOSe (%s)' % ('@201'))  # Set LNB 1 on
                time.sleep(2)
                E4440A.write(':CALCulate:MARKer1:ACTivate')
                E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
                E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
                level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
                print ("Level: %f") %level
                if level < -40:
                    time.sleep(1)
                    E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
                    level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
                    if level < -40:
                        #E4440A.write(':MMEMory:CDIRectory "%s"' % ('[USBDISK]:'))
                        Label = (RunName + '_%d_%d_low',(x,count))
                        print (" _%d_low: %d ************" % (x,level))
                        print Label
                        #E4440A.write(':MMEMory:STORe:IMAGe "%s"' % (str(Label)+'.png'))
                time.sleep(2)
##                marker1 = float(E4440A.query_ascii_values(':CALC:MARK1:FCO:X?'))
                for y in range(1, 5):
                    marker1 = float(E4440A.query(':CALC:MARK1:FCO:X?'))
                    time.sleep(1)
                    if marker1 > 1500000005:
                        print "------------------ FAIL -------------------"
                #marker1 = float(temp_values[0])
                    print marker1
                v11713C.write(':ROUTe:OPEn (%s)' % ('@201'))  # Set LNB 1 off
            if x == 2:
                # status = Get_HTTP_Result("SP4TA:STATE:2") # Set switch A to position x
                v11713C.write(':ROUTe:CLOSe (%s)' % ('@202'))  # Set LNB 2 on
                time.sleep(2)
                E4440A.write(':CALCulate:MARKer1:ACTivate')
                E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
                E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
                level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
                print ("Level: %f") %level
                if level < -40:
                    time.sleep(1)
                    E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
                    level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
                    if level < -40:
                        #E4440A.write(':MMEMory:CDIRectory "%s"' % ('[USBDISK]:'))
                        Label = (RunName + '_%d_%d_low',(x,count))
                        print (" _%d_low: %d ************" % (x,level))
                        print Label
                        #E4440A.write(':MMEMory:STORe:IMAGe "%s"' % (str(Label)+'.png'))
                time.sleep(2)
##                marker2 = float(E4440A.query_ascii_values(':CALC:MARK1:FCO:X?'))
                for y in range(1, 5):
                    marker2 = float(E4440A.query(':CALC:MARK1:FCO:X?'))
                    time.sleep(1)
                    if marker2 > 1500000005:
                        print "------------------ FAIL -------------------"
                #marker1 = float(temp_values[0])
                    print marker2
                v11713C.write(':ROUTe:OPEn (%s)' % ('@202'))  # Set LNB 2 off
            if x == 3:
                # status = Get_HTTP_Result("SP4TA:STATE:3") # Set switch A to position x
                v11713C.write(':ROUTe:CLOSe (%s)' % ('@203'))  # Set LNB 3 on
                time.sleep(2)
                E4440A.write(':CALCulate:MARKer1:ACTivate')
                E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
                E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
                level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
                print ("Level: %f") %level
                if level < -40:
                    time.sleep(1)
                    E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
                    level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
                    if level < -40:
                        #E4440A.write(':MMEMory:CDIRectory "%s"' % ('[USBDISK]:'))
                        Label = (RunName + '_%d_%d_low',(x,count))
                        print (" _%d_low: %d ************" % (x,level))
                        print Label
                        #E4440A.write(':MMEMory:STORe:IMAGe "%s"' % (str(Label)+'.png'))
                time.sleep(2)
##                marker3 = float(E4440A.query_ascii_values(':CALC:MARK1:FCO:X?'))
                for y in range(1, 5):
                    marker3 = float(E4440A.query(':CALC:MARK1:FCO:X?'))
                    time.sleep(1)
                    if marker3 > 1500000005:
                        print "------------------ FAIL -------------------"                
                    #marker1 = float(temp_values[0])
                    print marker3
                v11713C.write(':ROUTe:OPEn (%s)' % ('@203'))  # Set LNB 3 off
            if x == 4:
                # status = Get_HTTP_Result("SP4TA:STATE:4") # Set switch A to position x
                v11713C.write(':ROUTe:CLOSe (%s)' % ('@204'))  # Set LNB 4 on
                time.sleep(2)
                E4440A.write(':CALCulate:MARKer1:ACTivate')
                E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
                E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
                level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
                print ("Level: %f") %level
                if level < -40:
                    time.sleep(1)
                    E4440A.write(':CALCulate:MARKer1:FUNC:MAX')
                    level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
                    if level < -40:
                        #E4440A.write(':MMEMory:CDIRectory "%s"' % ('[USBDISK]:'))
                        Label = (RunName + '_%d_%d_low',(x,count))
                        print (" _%d_low: %d ************" % (x,level))
                        print Label
                        #E4440A.write(':MMEMory:STORe:IMAGe "%s"' % (str(Label)+'.png'))
                time.sleep(2)
##                marker4 = float(E4440A.query_ascii_values(':CALC:MARK1:FCO:X?'))
                for y in range(1, 5):
                    marker4 = float(E4440A.query(':CALC:MARK1:FCO:X?'))
                    time.sleep(1)
                    if marker4 > 1500000005:
                        print "------------------ FAIL -------------------"     
                    #marker1 = float(temp_values[0])
                    print marker4
                v11713C.write(':ROUTe:OPEn (%s)' % ('@204'))  # Set LNB 4 off

                #Get ready to write readings to file
                nowtim=calendar.timegm(time.gmtime())
                time_val = time.asctime( time.localtime(time.time()))
                temp_values = U3606B.query_ascii_values(':SOURce:SENSe:CURRent:LEVel?')
                current = temp_values[0]
                print("current= %f") %current

                temp_values = U3606B.query_ascii_values(':SOURce:SENSe:VOLTage:LEVel?')
                voltage = temp_values[0]
                print("voltage= %f") %voltage
                writer.writerow({'chan1Mkr': marker1,'chan2Mkr': marker2,'chan3Mkr': marker3,'chan4Mkr': marker4,'ChamTemp':temperature[0],'curr':current,'volt':voltage,'time':time_val,'timesec':nowtim})    
            print "Sw A connected, Com =>", Get_HTTP_Result("SP4TA:STATE?") # Print switch A position
##            status = Get_HTTP_Result("SP4TA:STATE:0") # Set switch A to position 0
##            print "Sw A connected, Com =>", Get_HTTP_Result("SP4TA:STATE?") # Print switch A position
            print "Done."
            time.sleep(5)
            temperature = Get_Temp()
            print ("Temp: %f") %temperature[0]
            if((nowtim-starttime)>10800):
                if(temperature >88.0):
            #if((nowtim-starttime)>1800):
               #v11713C = rm.open_resource('TCPIP0::10.0.10.67::inst0::INSTR')
                    v11713C.write(':ROUTe:CLOSe (%s)' % ('@110'))  # Set 10 MHz On
                    print("10MHz turned on")
            

##            E4440A.write(':MMEMory:CDIRectory "%s"' % ('[USBDISK]:'))
####        E4440A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##            E4440A.write(':MMEMory:STORe:IMAGe "%s"' % (str(count)+'.png'))

            count = count + 1

    E4440A.close()
    U3606B.close()
    v11713C.close()
    rm.close()



# end of Untitled
