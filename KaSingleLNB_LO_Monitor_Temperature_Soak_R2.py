#Orbtial Research Ltd.
#Imported from KaLNBLOMon-wPSoak_V1.0_Ass#5 PC_119_py3.9_new.py
#Written by Benjamin Stadnik
#2022-05-18
#Python 3.9

import pyvisa as visa
import time, csv, sys, os
import socket
import openpyxl as pyxl
import datetime
import tkinter as tk

#Equipment Addresses
TEMPERATURE_SENSOR_ADDRESS = ('10.0.10.119', 2000)
SA_VISA = 'GPIB0::18::INSTR'
SWITCH_VISA = 'TCPIP0::10.0.10.62::inst0::INSTR'
AMM_VISA = 'GPIB6::24::INSTR'
LXI_VISA = 'TCPIP0::10.0.10.62::inst0::INSTR'

#Testing Parameters
REF_FILE = 'Reference File R3.xlsx'
CENTER_FREQ = 1500000000                                #Hz
TIME_CHECKPOINT = {'Soak': 2, 'Ref_Enable': 3, 'End': 5}  #hours
HIGH_TEMPERATURE_THRESHOLD = 85                         #degrees Celsius


def main():
    global TIME_START
    TIME_START = timestamp()
    print(F'Time Start: {TIME_START[0]}')

    unit_list = []
    unit_list.append(M_Text.get())
    unit_list.append(N_Text.get())
    unit_list.append(O_Text.get())
    unit_list.append(P_Text.get())
    unit_list.append(Q_Text.get())
    unit_list.append(R_Text.get())
    unit_list.append(S_Text.get())
    unit_list.append(T_Text.get())

    filename = F'KaLNB_Soak_{TIME_START[1]}.xlsx'

    header = ['Unit_M_Freq','Unit_N_Freq','Unit_O_Freq','Unit_P_Freq',
                'Unit_Q_Freq','Unit_R_Freq','Unit_S_Freq','Unit_T_Freq',
                'Temperature (C)','Current (A)','Time','Time Elapsed', 
                "M","N","O","P","Q","R","S","T"] #unused list, just for reference

    #Create .xlsx file
    reference_file = pyxl.load_workbook(REF_FILE)
    wb = pyxl.Workbook()
    wb = reference_file
    #write tester, resource, unit info
    ws = wb['Info']
    ws['B1'] = YouText.get()
    ws['B2'] = os.path.basename(__file__)
    ws['B3'] = TIME_START[0]
    ws['B4'] = unit_list[0]
    ws['B5'] = unit_list[1]
    ws['B6'] = unit_list[2]
    ws['B7'] = unit_list[3]
    ws['B8'] = unit_list[4]
    ws['B9'] = unit_list[5]
    ws['B10'] = unit_list[6]
    ws['B11'] = unit_list[7]

    index = 12
    for values in Resources.values():
        B = 'B' + str(index)
        ws[B] = values
        index += 1
    
    wb.save(filename=filename)

    # Set up initial state of switches
    LXI.write(':ROUTe:OPEn:ALL')
    LXI.write(':ROUTe:CLOSe (%s)' % ('@208'))
    LXI.write(':ROUTe:CLOSe (%s)' % ('@110')) #Turn off (measuring OFFset untill temp is at 90C) 
 
    SA.write(':CALCulate:MARKer:FCOunt:RESolution:AUTO ON')    
    
    #Initial sweep
    print("***Start Initial Scan***")
    LO_locked = False
    LXI.write(':ROUTe:CLOSe (%s)' % ('@209')) #Turn on SSR
    time.sleep(5)
    Cycle(unit_list, filename, LO_locked)
    print("***Done Initial Scan***")

    LXI.write(':ROUTe:OPEn (%s)' % ('@209')) # Turn off SSR  
    now = timestamp()
    delta = datetime.timedelta(hours=TIME_CHECKPOINT['Soak']) 
    print(F"Turning off power and soaking the units for {str(delta)}")
    while((now[3]-TIME_START[3]) < delta):        
        now = timestamp()
        temperature = Get_Temp()
        print (F"Temperature: {temperature} C; Time remaining: {str(TIME_START[3]+delta-now[3])}")
        time.sleep(120)
    print("Soaking Complete. Resume Testing.")
    
    #Main Sweep
    LXI.write(':ROUTe:CLOSe (%s)' % ('@209'))
    time.sleep(5)
    High_Temperature_Flag = False
    while(1): 
        now, temperature = Cycle(unit_list, filename, LO_locked)
        time.sleep(120)
        if((now[3]-TIME_START[3]) > datetime.timedelta(hours=TIME_CHECKPOINT['Ref_Enable']) and (temperature > HIGH_TEMPERATURE_THRESHOLD) and not High_Temperature_Flag ):                        
            SA.write(':CALCulate:MARKer:FCOunt:RESolution:AUTO OFF')                                        
            SA.write(':CALCulate:MARKer:FCOunt:RESolution 1')                                               
            LXI.write(':ROUTe:OPEn (%s)' % ('@110'))
            LO_locked = False
            print(F"***10MHz reference activated {now[0]}***\n")
            High_Temperature_Flag = True                             
        if((now[3]-TIME_START[3]) > datetime.timedelta(hours=TIME_CHECKPOINT['End'])): 
            LXI.write(':ROUTe:OPEn (%s)' % ('@209'))
            LXI.write(':ROUTe:CLOSe (%s)' % ('@110'))
            print(F'***TEST COMPLETE*** {now[0]}')
            sys.exit()
 

def Cycle(unit_list ,filename, LO_locked):
    #Get time
    timer = timestamp()
    print(F"Time: {timer[0]}")

    #Fetch temperature
    temperature = float(Get_Temp())
    print(F"Temperature: {temperature} C")

    #Fetch DC Current Value
    current = round(AMM.query_ascii_values('current')[0],3) 
    print(F"Current: {current} A")
    #Gather frequency markers for each unit      
    results = []
    for index, value in enumerate(unit_list):
        if(value == ''):
            results.append('')
        else:
            if(index == 7):
                freq = Check_Unit(index+1,True,LO_locked)
            else:
                freq = Check_Unit(index+1,False,LO_locked)
            results.append(freq)

    #Calculate results with freqeuncy offset
    results_offset = []
    for index,value in enumerate(results):
        if(value == ''):
            results_offset.append('')
        else:
            results_offset.append(abs(CENTER_FREQ - results[index]))

    final = results + [temperature,current,timer[0],str(timer[3]-TIME_START[3])] + results_offset

    #Save results to workbook
    wb = pyxl.load_workbook(filename)
    ws = wb['Data']
    ws.append(final)
    wb.save(filename=filename)
    print('')

    return timer, temperature


def Check_Unit(number,inverted, LO_locked):
    pwr_threshold = -40
    freq_threshold = 5
    unit = [None,"M","N","O","P","Q","R","S","T"]

    if(inverted):
        LXI.write(F':ROUTe:OPEn (@20{number})')
    else:
        LXI.write(F':ROUTe:CLOSe (@20{number})')

    time.sleep(4)
    SA.write(':INITiate:PAUSe')

    SA.write(':CALC:MARK1:MAX')
    power = float(SA.query(':CALC:MARK1:Y?'))
    freq = float(SA.query(':CALC:MARK1:FCO:X?'))
    print(F"Unit {unit[number]}:")
    print(F"    Power: {power} dBm")
    print(F"    Frequency: {freq} Hz")
    if(power < pwr_threshold):
        print(F'***WARNING: Unit {unit[number]} detect low power')
    if(abs(CENTER_FREQ - freq) > freq_threshold and LO_locked):
        print(F'***WARNING: Unit {unit[number]} frequency offset from center frequency greater than {freq_threshold} Hz')
    
    SA.write(':INITiate:RESume')
    if(inverted):
        LXI.write(F':ROUTe:CLOSe (@20{number})')
    else:
        LXI.write(F':ROUTe:OPEn (@20{number})')

    return freq

def Get_Temp(): 
    message = '*SRHC\r'
    message_byte = str.encode(message)
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(TEMPERATURE_SENSOR_ADDRESS)       
        sock.sendall(message_byte)
        amount_received = 0
        amount_expected = len(message)        
        while amount_received < amount_expected:
            data = sock.recv(16)
            amount_received += len(data)        
    finally:
        sock.close()
        T_Chamber = data.strip()
        T_Chamber.lstrip()
        T_Chamberstr = T_Chamber.decode()
        
    return T_Chamberstr

def timestamp():
    now = []
    current_time = datetime.datetime.now()
    now.append(current_time.strftime("%Y %b %d %H:%M:%S")) #Easy to read
    now.append(current_time.strftime("%Y%b%d_%H%M%S")) #Append to file name
    now.append(current_time.strftime("%H%M%S%f")) #Milliseconds
    now.append(current_time) #Datetime format for time operations
    return now

def Equipment_Init(): 
    global rm, Resources, SA, LXI, AMM, TSensor
    Resources = {}
    error_str = ' Communication Error. Test functionality may be limited. Press Enter to acknowledge and continue without resource.'
    rm = visa.ResourceManager()
    try:
        SA = rm.open_resource(SA_VISA)
        SA.timeout = 10000
        Resources['SA'] = SA.query('*IDN?').split('\n')[0]
    except:
        input('Spectrum Analyzer' + error_str)
        Resources['SA'] = None
    try:
        LXI = rm.open_resource(LXI_VISA)
        LXI.timeout = 10000
        Resources['LXI'] = LXI.query('*IDN?').split('\n')[0]
    except:
        input('LXI' + error_str)
        Resources['LXI'] = None
    try:
        AMM = rm.open_resource(AMM_VISA)
        AMM.timeout = 10000
        Resources['AMM'] = 'HP 3478A 2545A24377'
    except:
        input('AMM' + error_str)
        Resources['AMM'] = None
    try:
        reply = Get_Temp()
        Resources['TSensor'] = 'Omega iTCX-W 18010043'
    except:
        input('TSensor' + error_str)
        Resources['TSensor'] = None

    print("Equipment List:")
    print("----------------------")
    for keys,values in Resources.items():
        print(str(keys)+' : '+ str(values))
    print("----------------------")
    print('\n' + 'Launching Test GUI')

def GUI():
    global master
    master = tk.Tk() ## Initialize Menu variable
    master.title(os.path.basename(__file__))
    master.geometry('440x250')

    #Variable List
    global M_Text, N_Text, O_Text, P_Text, Q_Text, R_Text, S_Text, T_Text, YouText

    #Unit Menu
    UnitFrame = tk.LabelFrame(master,text="Unit Serial Numbers:")
    pad=10
    tk.Label(UnitFrame,text='M:').grid(row=1,column=1,sticky='E',padx=pad)
    tk.Label(UnitFrame,text='N:').grid(row=2,column=1,sticky='E',padx=pad)
    tk.Label(UnitFrame,text='O:').grid(row=3,column=1,sticky='E',padx=pad)
    tk.Label(UnitFrame,text='P:').grid(row=4,column=1,sticky='E',padx=pad)
    tk.Label(UnitFrame,text='Q:').grid(row=1,column=3,sticky='E',padx=pad)
    tk.Label(UnitFrame,text='R:').grid(row=2,column=3,sticky='E',padx=pad)
    tk.Label(UnitFrame,text='S:').grid(row=3,column=3,sticky='E',padx=pad)
    tk.Label(UnitFrame,text='T:').grid(row=4,column=3,sticky='E',padx=pad)
    M_Text = tk.Entry(UnitFrame)
    N_Text = tk.Entry(UnitFrame)
    O_Text = tk.Entry(UnitFrame)
    P_Text = tk.Entry(UnitFrame)
    Q_Text = tk.Entry(UnitFrame)
    R_Text = tk.Entry(UnitFrame)
    S_Text = tk.Entry(UnitFrame)
    T_Text = tk.Entry(UnitFrame)
    M_Text.grid(row=1,column=2,sticky='W')
    N_Text.grid(row=2,column=2,sticky='W')
    O_Text.grid(row=3,column=2,sticky='W')
    P_Text.grid(row=4,column=2,sticky='W')
    Q_Text.grid(row=1,column=4,sticky='W')
    R_Text.grid(row=2,column=4,sticky='W')
    S_Text.grid(row=3,column=4,sticky='W')
    T_Text.grid(row=4,column=4,sticky='W')
    UnitFrame.pack(fill='both',ipadx=5,ipady=5,padx=5,pady=5,expand=False)
    
    #Information Frame
    InfoFrame = tk.LabelFrame(master,text="Information:")
    tk.Label(InfoFrame,text='Tester:').grid(row=1,column=1,sticky='E')
    YouText = tk.Entry(InfoFrame)
    YouText.grid(row=1,column=2,sticky='W')
    InfoFrame.pack(fill='both',ipadx=5,ipady=5,padx=5,pady=5,expand=False)
            
    #Button to Begin Testing
    width = 10

    Stop = tk.Label(master,text ='Press CTRL+C to stop test.').pack(fill='both',expand=False) 
    tk.Button(master,text='BEGIN',command=main,width=width).pack(ipadx=5,ipady=5,padx=5,pady=5,expand=False)

    master.mainloop()

def Catch_Exception(exception):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    error  = [exc_type ,fname,exc_tb.tb_lineno,exception]
    
    return error

def intermediate():
    try:
        main()
    except Exception as e:
        error = Catch_Exception(e)
        print(error)
        sys.exit()

Equipment_Init()
GUI()

#Revision History
#-------------------------------------------------------------------------------------------------------------
#2022-01-18 - (KaLNBLOMon-wPSoak_V1.0.py) - author BL for Ka Single LO temperature test in prodcution building 
#2022-05-18 - KaSingleLNB_LO_Monitor_Temperature_Soak_R2.py - Benjamin updated with GUI and cleaned up code
#-------------------------------------------------------------------------------------------------------------
