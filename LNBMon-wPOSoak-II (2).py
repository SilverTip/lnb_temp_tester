import visa
import time
import csv
import urllib2
import sys
import calendar
import socket

# start of Untitled
rm = visa.ResourceManager()

########################################
# Define a function to send an HTTP command and get the result
########################################
def Get_HTTP_Result(CmdToSend):
# Specify the IP address
    CmdToSend = "http://10.0.10.76/:" + CmdToSend
    # Send the HTTP command and try to read the result
    try:
        HTTP_Result = urllib2.urlopen(CmdToSend)
        PTE_Return = HTTP_Result.read()
        # The switch displays a web GUI for unrecognised commands
        if len(PTE_Return) > 100:
          print "Error, command not found:", CmdToSend
          PTE_Return = "Invalid Command!"
    # Catch an exception if URL is incorrect (incorrect IP or disconnected)
    except:
        print "Error, no response from device; check IP address and connections."
        PTE_Return = "No Response!"
        sys.exit() # Exit the script
    # Return the response
    return PTE_Return

##################
# Omega iServer Dual Temp Sensor
# This function polls and returns the temperature

# Connect the socket to the port where the server is listening
server_address = ('10.0.10.117', 1000)
message = '*SRHC\r'


# print 'connecting to %s port %s' % server_address
# sock.close()
# sock.connect(server_address)
def Get_Temp():
    global data
    try:
        #print 'connecting to %s port %s' % server_address
        # sock.close()
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(server_address)
        # Send data

        #print 'sending "%s"' % message
        sock.sendall(message)

        # Look for the response
        amount_received = 0
        amount_expected = len(message)

        while amount_received < amount_expected:
            data = sock.recv(16)
            amount_received += len(data)
            #print 'Temp: "%s"' % data


    finally:
        #print 'closing socket'
        sock.close()
    return data

def get_marker():
    E4440A.write(':CALCulate:MARKer1:ACTivate')
    E4440A.write(':CALCulate:MARKer1:MAXimum')
    E4440A.write(':CALCulate:MARKer1:MAXimum')
    level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
    print ("Level: %f") %level
    if level < -40:
        time.sleep(1)
        E4440A.write(':CALCulate:MARKer1:MAXimum')
        level = float(E4440A.query(':CALCulate:MARKer1:Y?'))
        if level < -40:
            #E4440A.write(':MMEMory:CDIRectory "%s"' % ('[USBDISK]:'))
            #Label = (RunName + '_%d_%d_low',(x,count))
            print (" _%d_low: %d ************" % (x,level))
            #print Label
            #E4440A.write(':MMEMory:STORe:IMAGe "%s"' % (str(Label)+'.png'))
    #time.sleep(2)
    ##                marker1 = float(E4440A.query_ascii_values(':CALC:MARK1:FCO:X?'))
    E4440A.write(':CALCulate:MARKer1:MAXimum')
    for y in range(1, 4):
        marker0 = float(E4440A.query(':CALC:MARK1:FCO:X?'))
        time.sleep(1)
        if marker0 > 1500000005:
            print "------------------ FAIL -------------------"
            E4440A.write(':CALCulate:MARKer1:MAXimum')
    #marker1 = float(temp_values[0])
        print marker0
    return marker0

count = 1
nowtim=calendar.timegm(time.gmtime())
RunName = raw_input("Enter name for data file: ")
RunName = RunName.replace('\r','')
filename = "%s%s.csv" %(RunName,nowtim)
print ('Filename for this run: %s') % filename
choice = raw_input("Continuing...")
choice = raw_input("Enter number of LNBs[1-8]: ")
choice = choice.replace('\r','')
print ("Your choice is: %s") % choice
NumLNB = int(choice)
print ("Num LNBs: %d") % NumLNB

#NumLNB = int(NumLNBstr)

temperature = Get_Temp()
print ("Temp: %s") %temperature

print (filename)
#time.sleep(100)

with open( '%s.csv' % filename, 'a',0) as csvfile:
    fieldnames = ['chan1Mkr','chan2Mkr','chan3Mkr','chan4Mkr',
                  'chan5Mkr','chan6Mkr','chan7Mkr','chan8Mkr',
                  'ChamTemp','curr','volt','time','timesec']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    print ("Mar2018-AutoPowerOn wSleeps - E4440A: %s") %filename

    # Initialize instruments
    rm = visa.ResourceManager()
    #Temp Sensor
    #v34405A = rm.open_resource('USB0::0x0957::0x0618::MY53090034::0::INSTR')
    # Power Supply / DMM
    U3606B = rm.open_resource('GPIB0::25::INSTR')
    # E4440A Spec Ann
    E4440A = rm.open_resource('TCPIP0::10.0.10.49::inst0::INSTR')
    E4440A.timeout = 50000
    E4440A.write(':CALCulate:MARKer1:FCOunt ON')    #Turn on Marker Counter
    # 11713C Switch/Attenuator Controller
    v11713C = rm.open_resource('TCPIP0::10.0.10.67::inst0::INSTR')

    # Set up Power Supply
    U3606B.write(':OUTPut:STATe %d' % (0))
    temp_values = U3606B.query_ascii_values(':OUTPut:STATe?')
    state = int(temp_values[0])
    U3606B.write(':OUTPut:STATe %d' % (1))
    temp_values = U3606B.query_ascii_values(':OUTPut:STATe?')
    state1 = int(temp_values[0])
    print("PS state= %d") %state1
    time.sleep(1)
    temp_values = U3606B.query_ascii_values(':SOURce:SENSe:CURRent:LEVel?')
    current = temp_values[0]
    print("SSR current= %f") %current
    temp_values = U3606B.query_ascii_values(':MEASure:CURRent:DC?')
    current = temp_values[0]
    print("LNB current= %f") %current
    temp_values = U3606B.query_ascii_values(':SOURce:SENSe:VOLTage:LEVel?')
    voltage = temp_values[0]
    print("voltage= %f") %voltage

    # Set up initial state of switches
    v11713C.write(':ROUTe:OPEn:ALL')
    v11713C.write(':ROUTe:CLOSe (%s)' % ('@208'))
    # Now everything is off
    #Turn off 10 MHz (measuring OFFset untill temp is at 90C)
    v11713C.write(':ROUTe:CLOSe (%s)' % ('@110'))
    #Turn on LNB 1
    v11713C.write(':ROUTe:CLOSe (%s)' % ('@201'))

    
    time_val = time.asctime( time.localtime(time.time()))
    starttime=calendar.timegm(time.gmtime())
    print("Start Time: %s") %time_val
          
    ##Do one cycle with power on
    marker1 = marker2 = marker3 = marker4 = marker5 = marker6 = marker7 = marker8 = 0.0
    for x in range(1, (NumLNB+1)):
        print "Initial loop LNB:%d" % (x)
        ########################################
        # Send some commands to the switch box
        ########################################

        if x == 1:
            #status = Get_HTTP_Result("SP4TA:STATE:1") # Set switch A to position x
            v11713C.write(':ROUTe:CLOSe (%s)' % ('@201')) # Set LNB 1 on
            time.sleep(2)
            marker1 = get_marker()
            v11713C.write(':ROUTe:OPEn (%s)' % ('@201'))  # Set LNB 1 off
        if x == 2:
            #status = Get_HTTP_Result("SP4TA:STATE:2") # Set switch A to position x
            v11713C.write(':ROUTe:CLOSe (%s)' % ('@202'))  # Set LNB 2 on
            time.sleep(2)
            marker2 = get_marker()
            v11713C.write(':ROUTe:OPEn (%s)' % ('@202'))  # Set LNB 2 off
        if x == 3:
            #status = Get_HTTP_Result("SP4TA:STATE:3") # Set switch A to position x
            v11713C.write(':ROUTe:CLOSe (%s)' % ('@203'))  # Set LNB 3 on
            time.sleep(2)
            marker3 = get_marker()
            v11713C.write(':ROUTe:OPEn (%s)' % ('@203'))  # Set LNB 3 off
        if x == 4:
            #status = Get_HTTP_Result("SP4TA:STATE:4") # Set switch A to position x
            v11713C.write(':ROUTe:CLOSe (%s)' % ('@204'))  # Set LNB 4 on
            time.sleep(2)
            marker4 = get_marker()
            v11713C.write(':ROUTe:OPEn (%s)' % ('@204'))  # Set LNB 4 off
        if x == 5:
            # status = Get_HTTP_Result("SP4TA:STATE:4") # Set switch A to position x
            v11713C.write(':ROUTe:CLOSe (%s)' % ('@205'))  # Set LNB 5 on
            time.sleep(2)
            marker5 = get_marker()
            v11713C.write(':ROUTe:OPEn (%s)' % ('@205'))  # Set LNB 5 off
        if x == 6:
            # status = Get_HTTP_Result("SP4TA:STATE:4") # Set switch A to position x
            v11713C.write(':ROUTe:CLOSe (%s)' % ('@206'))  # Set LNB 6 on
            time.sleep(2)
            marker6 = get_marker()
            v11713C.write(':ROUTe:OPEn (%s)' % ('@206'))  # Set LNB 6 off
        if x == 7:
            # status = Get_HTTP_Result("SP4TA:STATE:4") # Set switch A to position x
            v11713C.write(':ROUTe:CLOSe (%s)' % ('@207'))  # Set LNB 7 on
            time.sleep(2)
            marker7 = get_marker()
            v11713C.write(':ROUTe:OPEn (%s)' % ('@207'))  # Set LNB 7 off
        if x == 8:
            # status = Get_HTTP_Result("SP4TA:STATE:4") # Set switch A to position x
            v11713C.write(':ROUTe:OPEn (%s)' % ('@208'))  # Set LNB 4 on
            time.sleep(2)
            marker8 = get_marker()
            v11713C.write(':ROUTe:CLOSe (%s)' % ('@208'))  # Set LNB 8 off
    #############################################
    # Get Data for saving initial scan to file
    #
    nowtim=calendar.timegm(time.gmtime())
    time_val = time.asctime( time.localtime(time.time()))
    #temp_values = U3606B.query_ascii_values(':SOURce:SENSe:CURRent:LEVel?')
    temp_values = U3606B.query_ascii_values(':MEASure:CURRent:DC?')
    current = temp_values[0]
    print("LNB current= %f") %current

    temp_values = U3606B.query_ascii_values(':SOURce:SENSe:VOLTage:LEVel?')
    voltage = temp_values[0]
    print("voltage= %f") %voltage
    temperature = Get_Temp()
    print ("Temp: %s") %temperature
    writer.writerow({'chan1Mkr': marker1, 'chan2Mkr': marker2, 'chan3Mkr': marker3, 'chan4Mkr': marker4,
                     'chan5Mkr': marker5, 'chan6Mkr': marker6, 'chan7Mkr': marker7, 'chan8Mkr': marker8,
                     'ChamTemp': temperature, 'curr': current, 'volt': voltage, 'time': time_val,
                     'timesec': nowtim})

        ##            status = Get_HTTP_Result("SP4TA:STATE:0") # Set switch A to position 0
##            print "Sw A connected, Com =>", Get_HTTP_Result("SP4TA:STATE?") # Print switch A position
    print "Done Initial Scan."
    time.sleep(5)
        
    U3606B.write(':OUTPut:STATe %d' % (0))
    temp_values = U3606B.query_ascii_values(':OUTPut:STATe?')
    state = int(temp_values[0])
    nowtim=calendar.timegm(time.gmtime())
    time_val = time.asctime( time.localtime(time.time()))   
    ##sleep for 2 hrs
    print("Turning off power and Soaking for 2 hrs")
    while((nowtim-starttime)<7200):
        nowtim=calendar.timegm(time.gmtime())
        temperature = Get_Temp()
        print ("Temp: %s") %temperature
        time.sleep(5)
    #time.sleep(7200)
    print("Continuing")
    U3606B.write(':OUTPut:STATe %d' % (1))
    temp_values = U3606B.query_ascii_values(':OUTPut:STATe?')
    state1 = int(temp_values[0])
    print("PS state= %d") %state1
    
    while (1):
        #####
        ###turn off power supply for 20 seconds
        temperature = Get_Temp()
        print ("Temp: %s") %temperature

        #Start main loop - poll the LNBs
        marker1 = marker2 = marker3 = marker4 = marker5 = marker6 = marker7 = marker8 = 0.0
        for x in range(1, (NumLNB+1)):
            print "Main loop LNB:%d" % (x)
            ########################################
            # Send some commands to the switch box
            ########################################

            if x == 1:
                # status = Get_HTTP_Result("SP4TA:STATE:1") # Set switch A to position x
                v11713C.write(':ROUTe:CLOSe (%s)' % ('@201'))  # Set LNB 1 on
                time.sleep(2)
                marker1 = get_marker()
                v11713C.write(':ROUTe:OPEn (%s)' % ('@201'))  # Set LNB 1 off
            if x == 2:
                # status = Get_HTTP_Result("SP4TA:STATE:2") # Set switch A to position x
                v11713C.write(':ROUTe:CLOSe (%s)' % ('@202'))  # Set LNB 2 on
                time.sleep(2)
                marker2 = get_marker()
                v11713C.write(':ROUTe:OPEn (%s)' % ('@202'))  # Set LNB 2 off
            if x == 3:
                # status = Get_HTTP_Result("SP4TA:STATE:3") # Set switch A to position x
                v11713C.write(':ROUTe:CLOSe (%s)' % ('@203'))  # Set LNB 3 on
                time.sleep(2)
                marker3 = get_marker()
                v11713C.write(':ROUTe:OPEn (%s)' % ('@203'))  # Set LNB 3 off
            if x == 4:
                # status = Get_HTTP_Result("SP4TA:STATE:4") # Set switch A to position x
                v11713C.write(':ROUTe:CLOSe (%s)' % ('@204'))  # Set LNB 4 on
                time.sleep(2)
                marker4 = get_marker()
                v11713C.write(':ROUTe:OPEn (%s)' % ('@204'))  # Set LNB 4 off
            if x == 5:
                # status = Get_HTTP_Result("SP4TA:STATE:4") # Set switch A to position x
                v11713C.write(':ROUTe:CLOSe (%s)' % ('@205'))  # Set LNB 5 on
                time.sleep(2)
                marker5 = get_marker()
                v11713C.write(':ROUTe:OPEn (%s)' % ('@205'))  # Set LNB 5 off
            if x == 6:
                # status = Get_HTTP_Result("SP4TA:STATE:4") # Set switch A to position x
                v11713C.write(':ROUTe:CLOSe (%s)' % ('@206'))  # Set LNB 6 on
                time.sleep(2)
                marker6 = get_marker()
                v11713C.write(':ROUTe:OPEn (%s)' % ('@206'))  # Set LNB 6 off
            if x == 7:
                # status = Get_HTTP_Result("SP4TA:STATE:4") # Set switch A to position x
                v11713C.write(':ROUTe:CLOSe (%s)' % ('@207'))  # Set LNB 7 on
                time.sleep(2)
                marker7 = get_marker()
                v11713C.write(':ROUTe:OPEn (%s)' % ('@207'))  # Set LNB 7 off
            if x == 8:
                # status = Get_HTTP_Result("SP4TA:STATE:4") # Set switch A to position x
                v11713C.write(':ROUTe:OPEn (%s)' % ('@208'))  # Set LNB 8 on
                time.sleep(2)
                marker8 = get_marker()
                v11713C.write(':ROUTe:CLOSe (%s)' % ('@208'))  # Set LNB 8 off

        #Get ready to write readings to file
        nowtim=calendar.timegm(time.gmtime())
        time_val = time.asctime( time.localtime(time.time()))
        #temp_values = U3606B.query_ascii_values(':SOURce:SENSe:CURRent:LEVel?')
        temp_values = U3606B.query_ascii_values(':MEASure:CURRent:DC?')
        current = temp_values[0]
        print("LNB current= %f") %current

        temp_values = U3606B.query_ascii_values(':SOURce:SENSe:VOLTage:LEVel?')
        voltage = temp_values[0]
        print("voltage= %f") %voltage
        writer.writerow({'chan1Mkr': marker1,'chan2Mkr': marker2,'chan3Mkr': marker3,'chan4Mkr': marker4,
                         'chan5Mkr': marker5,'chan6Mkr': marker6,'chan7Mkr': marker7,'chan8Mkr': marker8,
                         'ChamTemp':temperature,'curr':current,'volt':voltage,'time':time_val,'timesec':nowtim})
#            print "Sw A connected, Com =>", Get_HTTP_Result("SP4TA:STATE?") # Print switch A position
##            status = Get_HTTP_Result("SP4TA:STATE:0") # Set switch A to position 0
##            print "Sw A connected, Com =>", Get_HTTP_Result("SP4TA:STATE?") # Print switch A position
        print "Done loop. Write to file."
        time.sleep(5)
        temperature = Get_Temp()
        print ("Temp: %s") %temperature
        if((nowtim-starttime)>10800):
            if(float(temperature) >88.0):
        #if((nowtim-starttime)>1800):
           #v11713C = rm.open_resource('TCPIP0::10.0.10.67::inst0::INSTR')
                v11713C.write(':ROUTe:OPEn (%s)' % ('@110'))  # Set 10 MHz On
                print("10MHz turned on")


##            E4440A.write(':MMEMory:CDIRectory "%s"' % ('[USBDISK]:'))
####        E4440A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##            E4440A.write(':MMEMory:STORe:IMAGe "%s"' % (str(count)+'.png'))

        count = count + 1

    E4440A.close()
    U3606B.close()
    v11713C.close()
    rm.close()



# end of Untitled
