import visa
import time
import csv
import urllib2
import sys
import calendar
import socket


# start of Untitled

# Create a TCP/IP socket


# Connect the socket to the port where the server is listening
server_address = ('10.0.10.117', 1000)


########################################
# Define a function to send an HTTP command and get the result
########################################
def Get_HTTP_Result(CmdToSend):
# Specify the IP address
    CmdToSend = "http://10.0.10.117:1000/" + CmdToSend
    # Send the HTTP command and try to read the result
    try:
        HTTP_Result = urllib2.urlopen(CmdToSend)
        PTE_Return = HTTP_Result.read()
        # The switch displays a web GUI for unrecognised commands
        if len(PTE_Return) > 100:
          print "Error, command not found:", CmdToSend
          PTE_Return = "Invalid Command!"
    # Catch an exception if URL is incorrect (incorrect IP or disconnected)
    except:
        print "Error, no response from device; check IP address and connections."
        PTE_Return = "No Response!"
        sys.exit() # Exit the script
    # Return the response
    return PTE_Return

message = '*SRHC\r'
#print 'connecting to %s port %s' % server_address
#sock.close()
#sock.connect(server_address)
def Get_Temp():
    try:
        print 'connecting to %s port %s' % server_address
        #sock.close()
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(server_address)       
        # Send data

        print 'sending "%s"' % message
        sock.sendall(message)

        # Look for the response
        amount_received = 0
        amount_expected = len(message)
        
        while amount_received < amount_expected:
            data = sock.recv(16)
            amount_received += len(data)
            print 'received "%s"' % data
        

    finally:
        print 'closing socket'
        sock.close()
    return data


while (1):
    print Get_Temp()
    time.sleep(1)
    


